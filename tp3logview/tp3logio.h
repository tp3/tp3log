#ifndef TP3LOGIO_H
#define TP3LOGIO_H

#include <QObject>

class tp3LogIO : public QObject
{
    Q_OBJECT
public:
    enum LogType {
        CurrentLog,
        PreviousLog,
        CustomFile,
    };
    Q_ENUM(LogType)

    explicit tp3LogIO(QObject *parent = 0);

    Q_PROPERTY(LogType logType READ logType WRITE setLogType NOTIFY logTypeChanged)
    Q_PROPERTY(QString source READ source WRITE setSource NOTIFY sourceChanged)

    Q_INVOKABLE QString read();
    Q_INVOKABLE bool write(const QString& data);

    QString source() { return mSource; }
    LogType logType() {return mLogType; }

public slots:
    void setSource(const QString& source) { mSource = source; }
    void setLogType(const LogType& logType) { mLogType = logType; }

signals:
    void sourceChanged(const QString& source);
    void logTypeChanged(const LogType& logType);
    void error(const QString& msg);

private:
    QString mSource;
    LogType mLogType = CurrentLog;
};

#endif // TP3LOGIO_H
