#include "tp3logview.h"

#ifdef QT_QML_LIB
#  include <QQmlEngine>
#endif

#include "tp3logio.h"

void tp3LogView::initialize() {
#ifdef QT_QML_LIB
    Q_INIT_RESOURCE(tp3logviewresources);
    qmlRegisterType<tp3LogIO>("Tp3LogIO", 1, 0, "Tp3LogIO");
#endif
}
