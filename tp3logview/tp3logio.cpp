#include "tp3logio.h"
#include "../tp3log/tp3log.h"

#include <QFile>
#include <QTextStream>

Q_DECLARE_METATYPE(tp3LogIO::LogType)

tp3LogIO::tp3LogIO(QObject *parent) : QObject(parent)
{
}

QString tp3LogIO::read()
{
    QString source = mSource;
    switch (mLogType) {
    case CurrentLog:
        source = tp3Log::logPath();
        break;
    case PreviousLog:
        source = tp3Log::oldLogPath();
        break;
    case CustomFile:
        break;
    }

    if (source.isEmpty()){
        emit error("source is empty");
        return QString();
    }

    QFile file(source);
    QString fileContent;
    if ( file.open(QIODevice::ReadOnly) ) {
        QTextStream t( &file );
        fileContent = t.readAll();

        file.close();
    } else {
        emit error("Unable to open the file");
        return QString();
    }
    return fileContent;
}

bool tp3LogIO::write(const QString& data)
{
    if (mSource.isEmpty())
        return false;

    QFile file(mSource);
    if (!file.open(QFile::WriteOnly | QFile::Truncate))
        return false;

    QTextStream out(&file);
    out << data;

    file.close();

    return true;
}
