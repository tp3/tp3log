import QtQuick 2.0
import QtQuick.Controls 2.1
import Tp3LogIO 1.0

Item {
    property alias logType: logFile.logType
    property alias text: text.text

    function refresh() {
        flickable.refresh()
    }

    Flickable {
        id: flickable
        anchors.fill: parent
        flickableDirection: Flickable.VerticalFlick
        boundsBehavior: Flickable.StopAtBounds

        function refresh() {
            text.text = logFile.read()
        }

        TextArea.flickable: TextArea {
            id: text
            text: logFile.read()
            wrapMode: TextArea.Wrap
            readOnly: true
        }

        ScrollBar.vertical: ScrollBar { }
    }

    Tp3LogIO {
        id: logFile
        logType: Tp3LogIO.CurrentLog;
        onError: console.log(msg)
    }
}
