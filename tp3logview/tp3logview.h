#ifndef TP3LOGVIEW_H
#define TP3LOGVIEW_H

///
/// \brief The tp3LogView class defines the interface to the application
///        that shall use the tp3LogView components.
///
///
/// \see tp3LogView::initialize()
///
class tp3LogView {
public:
    ///
    /// \brief Initialized the tp3LogView qml components
    ///
    /// Call this in your main function before initializing the
    /// qml engine.
    ///
    static void initialize();
};

#endif // TP3LOGVIEW_H
