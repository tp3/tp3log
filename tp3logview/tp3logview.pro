TEMPLATE = lib
TARGET = tp3logview
DESTDIR = $$shadowed($$PWD)/../tp3log/lib

qtHaveModule(qml):qtHaveModule(quick) {
    QT += qml quick
    RESOURCES += \
        tp3logviewresources.qrc
}

CONFIG += staticlib

HEADERS += \
    tp3logio.h \
    tp3logview.h

SOURCES += \
    tp3logio.cpp \
    tp3logview.cpp

