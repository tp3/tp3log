#ifndef TP3LOG_H
#define TP3LOG_H

#include <QDebug>
#include <QObject>
#include <QIODevice>
#include <QFile>
#include <QVector>
#include <QRegularExpression>

class tp3Debug;
class tp3DebugDevice;

class tp3Log : public QObject
{
    Q_OBJECT
public:
    enum LogLevel {
        LVL_FATAL = 0,
        LVL_WARNING,
        LVL_INFO,
        LVL_DEBUG,
        LVL_ALL
    };

    static tp3Log &instance() {
        static tp3Log log;
        return log;
    }

    static void setLogPath(QString path);
    static QString logPath();
    static QString oldLogPath();

    static QDebug debugStream(tp3Debug stream, QString tag, QString file, int line);

    static tp3Debug debug();
    static tp3Debug info();
    static tp3Debug warning();
    static tp3Debug fatal();

    static void include(QString regex) {
        instance().mIncludes.push_back(QRegularExpression(regex));
    }

    static void resetInclude() {
        instance().mIncludes.clear();
    }

    static void exclude(QString regex) {
        instance().mExcludes.push_back(QRegularExpression(regex));
    }

    static void resetExcludes() {
        instance().mExcludes.clear();
    }

    static LogLevel logLevel() {return instance().mLogLevel;}
    static void setLogLevel(const LogLevel level) {instance().mLogLevel = level;}

private:
    static bool ignore(tp3Debug &stream, QString tag);

    LogLevel mLogLevel = LVL_INFO;
    QVector<QRegularExpression> mIncludes;
    QVector<QRegularExpression> mExcludes;
};

class tp3Debug : public QDebug
{
public:
    tp3Debug(tp3DebugDevice *device);
    ~tp3Debug();

    tp3Log::LogLevel level() const;
private:
    tp3DebugDevice *mDevice;
};

class tp3ZeroDebug
{
public:
    template<typename T>
    inline tp3ZeroDebug &operator<<(T)
    {
        return *this;
    }
};

#define DEBUG tp3Log::debug()
#define INFO tp3Log::info()
#define WARNING tp3Log::warning()
#define FATAL tp3Log::fatal()

#define LOG(x) tp3Log::debugStream((x), QStringLiteral(__FILE__), QStringLiteral(__FILE__), __LINE__)
#define LOGT(x, tag) tp3Log::debugStream((x), tag, QStringLiteral(__FILE__), __LINE__)
#define LOGVA(x, m, ...)  {LOG(x) << QString::asprintf(m __VA_OPT__(,) __VA_ARGS__);}

#if defined(QT_DEBUG)
#define DLOG(x) LOG(x)
#define DLOGT(x, tag) LOG(x, tag)
#define DLOGVA(x, ...) LOGVA(x, ...)
#else
#define DLOG(x) tp3ZeroDebug()
#define DLOGT(x, tag) tp3ZeroDebug()
#define DLOGVA(x, ...)
#endif

#endif // TP3LOG_H
