#include "tp3log.h"

#include <QIODevice>
#include <QTextStream>
#include <QDir>


class tp3LogFile
{
public:
    ~tp3LogFile() {
        if (mStream) {
            delete mStream;
            mStream = nullptr;
        }
        if (mFile) {
            delete mFile;
            mFile = nullptr;
        }
    }

    void open(QString path) {
        if (path.isEmpty()) {
            qWarning() << "No log path secified!";
            return;
        }

        QDir wd;
        mFile = new QFile(wd.absoluteFilePath(path));
        if (mFile->exists()) {
            QString oldFilePath = wd.absoluteFilePath(oldLogPath());
            if (!QFile::remove(oldFilePath)) {
                qWarning() << "Could not delete old log";
            }
            if (!mFile->rename(oldFilePath)) {
                qWarning() << "Could not rename previous log";
                if (!mFile->remove()) {
                    qWarning() << "Could not remove previous log";
                }
            }
            delete mFile;
            mFile = new QFile(wd.absoluteFilePath(path));
        }

        mFile->open(QFile::WriteOnly);
        mStream = new QTextStream(mFile);
    }

    void addLine(QString line) {
        if (mStream) {
            *mStream << line << "\n";
            mStream->flush();
        }
    }

    QString logPath() {
        if (!mFile) {return QString();}
        return mFile->fileName();
    }

    QString oldLogPath() {
        if (!mFile) {return QString();}
        return mFile->fileName() + ".old";
    }

private:
    QFile *mFile = nullptr;
    QTextStream *mStream = nullptr;
};

const QString tp3LogLevelPrefixes[] = {
    "F",
    "W",
    "I",
    "D"
};


class tp3DebugDevice : public QIODevice
{
public:
    typedef QDebug (QMessageLogger::*logger)() const;
    tp3DebugDevice(logger qtDebug, tp3LogFile &file, tp3Log::LogLevel level)
        : mQtDebug(qtDebug)
        , mFile(file)
        , mLevel(level)
    {
        open(QIODevice::WriteOnly);
    }

    tp3Log::LogLevel level() const {
        return mLevel;
    }

protected:
    virtual qint64 writeData(const char *data, qint64 maxSize) override {
        QMessageLogger log(QT_MESSAGELOG_FILE, QT_MESSAGELOG_LINE, QT_MESSAGELOG_FUNC);

        ((log).*(mQtDebug))().noquote().nospace() << tp3LogLevelPrefixes[mLevel] << " " << QString::fromUtf8(data, maxSize);
        mFile.addLine(tp3LogLevelPrefixes[mLevel] + " " + QString::fromUtf8(data, maxSize));
        return maxSize;
    }

    virtual qint64 readData(char *data, qint64 maxSize) override {
        Q_UNUSED(data);
        Q_UNUSED(maxSize);
        return 0;
    }

private:
    logger mQtDebug;
    tp3LogFile &mFile;
    const tp3Log::LogLevel mLevel;
};

class tp3NullDevice : public QIODevice {
public:
    tp3NullDevice() {
        open(QIODevice::ReadWrite);
    }

protected:
    virtual qint64 writeData(const char *data, qint64 maxSize) override {
        Q_UNUSED(data);
        Q_UNUSED(maxSize);
        return maxSize;
    }

    virtual qint64 readData(char *data, qint64 maxSize) override {
        Q_UNUSED(data);
        Q_UNUSED(maxSize);
        return 0;
    }
};

tp3NullDevice gtp3NullDevice;
QDebug gtp3NullDebug(&gtp3NullDevice);

tp3LogFile gtp3LogFile;

tp3DebugDevice gtp3DebugStream(&QMessageLogger::debug, gtp3LogFile, tp3Log::LVL_DEBUG);
tp3DebugDevice gtp3InfoStream(&QMessageLogger::info, gtp3LogFile, tp3Log::LVL_INFO);
tp3DebugDevice gtp3WarningStream(&QMessageLogger::warning, gtp3LogFile, tp3Log::LVL_WARNING);
tp3DebugDevice gtp3FatalStream(&QMessageLogger::critical, gtp3LogFile, tp3Log::LVL_FATAL);

tp3Debug::tp3Debug(tp3DebugDevice *device)
    : QDebug(device)
    , mDevice(device)
{}

tp3Debug::~tp3Debug() {
    // TODO: consider flushing.
}

tp3Log::LogLevel tp3Debug::level() const {
    return mDevice->level();
}

void tp3Log::setLogPath(QString path) {
    gtp3LogFile.open(path);
}

QString tp3Log::logPath() {
    return gtp3LogFile.logPath();
}

QString tp3Log::oldLogPath() {
    return gtp3LogFile.oldLogPath();
}

QDebug tp3Log::debugStream(tp3Debug stream, QString tag, QString file, int line) {
    if (ignore(stream, tag)) {
        return gtp3NullDebug;
    } else {
        stream.noquote().nospace() << file << " (" << line << "):";
        return stream.quote().space();
    }
}

bool tp3Log::ignore(tp3Debug &stream, QString tag) {
    for (QRegularExpression &r : instance().mIncludes) {
        if (r.match(tag).hasMatch()) {
            return false;
        }
    }

    for (QRegularExpression &r : instance().mExcludes) {
        if (r.match(tag).hasMatch()) {
            return true;
        }
    }

    return stream.level() > instance().mLogLevel;
}

tp3Debug tp3Log::debug() {
    return tp3Debug(&gtp3DebugStream);
}

tp3Debug tp3Log::info() {
    return tp3Debug(&gtp3InfoStream);
}

tp3Debug tp3Log::warning() {
    return tp3Debug(&gtp3WarningStream);
}

tp3Debug tp3Log::fatal() {
    return tp3Debug(&gtp3FatalStream);
}
