TEMPLATE = lib
TARGET = tp3log
DESTDIR = $$shadowed($$PWD)/lib

CONFIG += staticlib

win32-msvc*:QMAKE_CXXFLAGS += /Zc:preprocessor

HEADERS += \
    tp3log.h

SOURCES += \
    tp3log.cpp
