TEMPLATE = subdirs
SUBDIRS += tp3log examples

examples.depends = tp3log

qtHaveModule(qml):qtHaveModule(quick){
    SUBDIRS += tp3logview
    tp3logview.depends += tp3log
    examples.depends += tp3logview
}
