#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "tp3log.h"
#include "tp3logview.h"

int main(int argc, char *argv[])
{
    tp3LogView::initialize();

    // set log path (usually to a temporary location, or AppDataDir
    tp3Log::setLogPath("log.txt");

    // Write some log messages
    LOG(INFO) << "This is an info message";
    LOG(WARNING) << "This is a warning message";
    LOG(FATAL) << "This is a fatal message. " << 0 << "You can use normal QDebug streams" << 0.243;
    LOG(DEBUG) << "Debug messages are usually ignored. This is not written to the log!";
    tp3Log::setLogLevel(tp3Log::LVL_ALL);
    LOG(DEBUG) << "This message is now written!";

    // Write messages for debug, they will be removed when compiling in release mode
    DLOG(INFO) << "This message will only be visible in debug mode";

    // the following line will ignore all messages from main.cpp
    tp3Log::exclude("main.cpp");
    LOG(WARNING) << "This is not written to the log, because it is excluded";

    // reset all and show how to use includes
    tp3Log::resetExcludes();
    tp3Log::setLogLevel(tp3Log::LVL_INFO);
    LOG(INFO) << "Only info messages will be shown";
    LOG(DEBUG) << "This message is hidden!";

    // now include main messages
    tp3Log::include("main.cpp");
    LOG(DEBUG) << "This messages is now shown!";

    // reset all
    tp3Log::resetExcludes();
    tp3Log::resetInclude();

    // show how to use custom tags
    LOGT(INFO, "TAG1") << "This will be written of TAG1";
    tp3Log::exclude("TAG1");
    LOGT(INFO, "TAG1") << "This will be ignored of TAG1.";
    LOGT(INFO, "TAG2") << "This will be written of TAG2.";
    LOG(INFO) << "This will be written of default tag = filename.";

    // show how to use var args log
    LOGVA(INFO, "This is c-like formatting of log messages");
    LOGVA(INFO, "This is a integer %03d with at least 3 digits filled with zeros", 21);
    LOGVA(INFO, "This is a float %.2f with two digits after the .", 0.2453);
    LOGVA(INFO, "This is string formatting '%s'", "Innter string");

    // Log viewer
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    return app.exec();
}
