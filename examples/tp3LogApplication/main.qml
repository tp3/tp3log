import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Page {
            Tp3CurrentLogView {
                anchors.fill: parent
            }
        }

        Page {
            Tp3PreviousLogView {
                anchors.fill: parent
            }
        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        TabButton {
            text: qsTr("Current log")
        }
        TabButton {
            text: qsTr("Old log")
        }
    }
}
